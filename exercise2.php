<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>The Invitation - Exercise 2</title>
    <link rel="stylesheet" href="animate.min.css">
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    <style>
        html, body {
            height:100%;
        }
        body {
            display:flex;
            align-items:center;
        }
    </style>
</head>
<body>
<div class="d-flex align-items-center flex-column justify-content-center h-100 bg-dark text-white md-offset col-md-offset-5 col-md-2" id="header">
    <h1 class="display-4">Exercise 2</h1>
    <p>
        Please fill in the location you've calculated to check whether or not it's correct.
    </p>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?? '#'; ?>" method="POST">
        <div class="form-group">
            <input class="form-control form-control-lg" placeholder="xx.xxxxxx x.xxxxxx" type="text" name="answer">
        </div>
        <div class="form-group">
            <button class="btn btn-info btn-lg btn-block" type="submit">Submit</button>
        </div>
    </form>
    <?php
    if(count($_POST) > 0 && isset($_POST["answer"])) {
        $result = filter_var($_POST["answer"], FILTER_SANITIZE_STRING);

        if(strtolower($result) === "53.208966 6.575892") {
            echo <<<HTML
<div class="alert alert-success animated bounce" role="alert">
  <strong>Well done!</strong>. 
</div>
HTML;
        } else {
            echo <<<HTML
<div class="alert alert-danger animated shake" role="alert">
  <strong>Oh snap!</strong> the given answer is incorrect.
</div>
HTML;

        }
    }
    ?>
</div>
</body>
</html>