<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>The Invitation - The code</title>
    <link rel="stylesheet" href="animate.min.css">
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script><style>
        html, body {
            height:100%;
        }
        body {
            display:flex;
            align-items:center;
        }
    </style>
</head>
<body>
<div class="d-flex align-items-center flex-column justify-content-center h-100 bg-dark text-white md-offset col-md-offset-5 col-md-2" id="header">
    <p>Please enter a code you've obtained.</p>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?? '#'; ?>" method="POST">
        <div class="form-group">
            <input class="form-control form-control-lg" placeholder="The code" type="text" name="code">
        </div>
        <div class="form-group">
            <button class="btn btn-info btn-lg btn-block" type="submit">Submit</button>
        </div>
    </form>
    <?php
    if(count($_POST) > 0 && isset($_POST["code"])) {
        $result = filter_var($_POST["code"], FILTER_SANITIZE_STRING);

        if($result === "MjUtMDktMTk4OQ==") {
            echo <<<HTML
<div class="alert alert-success animated bounce" role="alert">
  <strong>Well done!</strong> here is your first clue: '<strong>26-10-2017</strong>'. 
</div>
HTML;
        } else {
            echo <<<HTML
<div class="alert alert-danger animated shake" role="alert">
  <strong>Oh snap!</strong> this is an unknown code.
</div>
HTML;

        }
    }
    ?>
</div>
</body>
</html>