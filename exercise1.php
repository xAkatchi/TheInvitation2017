<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>The Invitation - Exercise 1</title>
    <link rel="stylesheet" href="animate.min.css">
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="tether.min.js"></script>
    <script src="bootstrap.min.js"></script>
    <style>
        html, body {
            height:100%;
        }
        body {
            display:flex;
            align-items:center;
        }
    </style>
</head>
<body>
<div class="d-flex align-items-center flex-column justify-content-center h-100 bg-dark text-white md-offset col-md-offset-5 col-md-2" id="header">
    <h1 class="display-4">Exercise 1</h1>
    <p>
        For this exercise, you can only succeed if you work together. <br>
        The goal is to find a word that can be created from all the letters that are available to you.
    </p>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?? '#'; ?>" method="POST">
        <div class="form-group">
            <input class="form-control form-control-lg" placeholder="The answer" type="text" name="answer">
        </div>
        <div class="form-group">
            <button class="btn btn-info btn-lg btn-block" type="submit">Submit</button>
        </div>
    </form>
    <?php
    if(count($_POST) > 0 && isset($_POST["answer"])) {
        $result = filter_var($_POST["answer"], FILTER_SANITIZE_STRING);

        if(strtolower($result) === "hamburger") {
            echo <<<HTML
<div class="alert alert-success animated bounce" role="alert">
  <strong>Well done!</strong> here is your first code <code>MjUtMDktMTk4OQ==</code>. You can enter this code <a href="code.php"><strong>here</strong></a>. 
</div>
HTML;
        } else {
            echo <<<HTML
<div class="alert alert-danger animated shake" role="alert">
  <strong>Oh snap!</strong> the given answer is incorrect.
</div>
HTML;

        }
    }
    ?>
</div>
</body>
</html>